'use strict';

const crypto = require('crypto');
const faker = require('faker');

// Création d'un user avec faker
const createFakeUser = () => ({
    login: faker.name.findName(),
    password: encrypt(faker.internet.password()),
    email: faker.internet.email(),
    firstname: faker.name.firstName(),
    lastname: faker.name.lastName(),
    company: faker.name.jobArea(),
    function: faker.name.jobType()
});

//Encryptage du mot de passe
function encrypt(secret){
    const hash = crypto.createHmac('sha256', secret)
    hash.update(secret);
    return hash.digest('base64');
}

//Insertion de 1000 users avec des paramètres fake et un mot de passe encrypté
exports.seed = async function(knex, Promise){
    const fakeUsers = [];
    const desiredFakeUsers = 1000;
    for (let i = 0; i < desiredFakeUsers; i++){
        fakeUsers.push(createFakeUser());
    }
    await knex('users')
        .insert(fakeUsers)
};


