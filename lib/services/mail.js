'use strict';

const { Service }        = require('schmervice');
const Mailgen = require('mailgen');
const nodemailer = require('nodemailer');



module.exports = class MailService extends Service {


    async initialize(){ // CALLED ON SERVER INITIALIZATION (onPreStart)
        require('dotenv').config();

        this.transporter = nodemailer.createTransport({
            service: process.env.NODEMAILER_SERVICE,
            auth: {
                user: process.env.ADR_MAIL,
                pass: process.env.MAIL_PASSWORD,
            }
        });


        this.mailGenerator = new Mailgen({
            theme: 'default',
            product: {
                // Appears in header & footer of e-mails
                name: 'BOILERPLATE - BRIQUET Clément',
                link: 'https://mailgen.js/'
            }
        });
    }

    async teardown(){ // CALLED ON SERVER STOP (OnPostStop)

        // tear down stuff here
    }

    testMailUser(emailAdress){ //Sends test mail
        var email = {
            body: {
                name: 'Test Mail User',
                intro: 'Mail de test User',
                action: {
                    instructions: 'Accès au git de test USER',
                    button: {
                        color: '#bc2222', // Optional action button color
                        text: 'BOILERPLATE Project',
                        link: 'https://gitlab.com/briqucle/boilerplate.git'
                    }
                },
                outro: 'Cordialement'
            }
        };

        // setup e-mail data with unicode symbols
        var mailOptions = {
            from: 'BOILERPLATE Project <'+process.env.ADR_MAIL+'>', // sender address
            to: 'MAIL USER' + ' <' + emailAdress+'>', // list of receivers
            subject: 'Email for test', // Subject line
            text: this.mailGenerator.generatePlaintext(email),
            html: this.mailGenerator.generate(email),
        };

        this.sendMailUser(mailOptions);
    }

    newAccountUser(emailAdress, name, login, password){
        var email = {
            body: {
                name: name,
                intro: 'Bienvenue dans BOILERPLATE. Votre compte se nomme : \nLogin: '+login+', Password: '+password,
                action: {
                    instructions: 'Accès au git de test USER',
                    button: {
                        color: '#bc2222', // Optional action button color
                        text: 'BOILERPLATE Project',
                        link: 'https://gitlab.com/briqucle/boilerplate.git'
                    }
                },
                outro: 'Cordialement'
            }
        };

        var mailOptions = {
            from: 'BOILERPLATE Project <'+process.env.ADR_MAIL+'>', // sender address
            to: name + ' <' + emailAdress+'>', // list of receivers
            subject: 'BOILERPLATE - Nouveau compte', // Subject line
            text: this.mailGenerator.generatePlaintext(email),
            html: this.mailGenerator.generate(email),
        };

        this.sendMailUser(mailOptions);
    }

    updateAccountUser(emailAdress, name){
        var email = {
            body: {
                name: name,
                intro: 'Votre compte a bien été mis à jour !',
                action: {
                    instructions: 'Accès au git de test USER',
                    button: {
                        color: '#bc2222', // Optional action button color
                        text: 'BOILERPLATE Project',
                        link: 'https://gitlab.com/briqucle/boilerplate.git'
                    }
                },
                outro: 'Cordialement'
            }
        };

        var mailOptions = {
            from: 'Hpal Project <'+process.env.ADR_MAIL+'>', // sender address
            to: name + ' <' + emailAdress+'>', // list of receivers
            subject: 'HapiPal - Account updated', // Subject line
            text: this.mailGenerator.generatePlaintext(email),
            html: this.mailGenerator.generate(email),
        };

        this.sendMailUser(mailOptions);
    }

    sendMailUser(mailOptions){
        // send mail with defined transport object
        this.transporter.sendMailUser(mailOptions, function(error, info){
            if(error){
                return console.error(error);
            }
            console.log('Message sent: ' + info.response);
        });
    }
}
