'use strict';

const Joi = require('joi'),
Boom = require('boom');
//Module pour récupérer un user selon son ID
module.exports = {
    method: 'GET',
    path: '/user/{id}',
    options: {
        handler: async (request, h) => {
            const { userService } = request.services()

            var user = await userService.getById(request.params.id);
            if (user.length === 0){
                return Boom.notFound('L\'utilisateur ' + request.params.id + ' est introuvable !')
            }
            else {
                return h.response(user[0]).code(200);
            }
        },

        tags:[
            'api'
        ],

        validate: {
            params:{
                id: Joi.number().integer().min(1).required()
            }
        }
    }
};
