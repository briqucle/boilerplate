'use strict';

const joi = require('joi'),
Boom = require('boom');
//Module pour s'authentifier
module.exports = {
    method: 'POST',
    path: '/auth',
    options: {
        handler: async (request, h) => {
            const { userService } = request.services()

            var user = await userService.getByLogin(request.payload.login)
            user = user[0];

            if (!user)
                return Boom.notFound("L'utilisateur " + request.payload.login + " est invalide ! Veuillez réessayer !.")

            if (await user.verifyPassword(request.payload.password))
                return h.response({msg : 'ok'}).code(200)
            else
                return h.response({msg : 'ko'}).code(403)
        },

        tags:[
            'api'
        ],

        validate: {
            payload: joi.object({
                login: joi.string().required(),
                password: joi.string().alphanum().required(),
            })
        }
    }
};
