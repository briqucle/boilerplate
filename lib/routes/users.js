'use strict';

module.exports = [{
    method: 'get',
    path: '/user',
    options: {
        handler: (request) => {

            const { userService } = request.services();
            return userService.hello({ firstName: 'John'});
        }
    }
},
    {
        method: 'get',
        path: '/users',
        options: {
            handler: (request) => {

                const { userService } = request.services();
                return userService.getUser();
            }
        }
    }
];


